import { createStore } from 'redux'
import { timerReducer } from './timerReducer'
import { compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

const store = createStore(timerReducer, compose(applyMiddleware(thunk), 
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()))

export default store