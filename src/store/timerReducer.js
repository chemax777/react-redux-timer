import { SET_TIMER, START_TIMER, RESET_TIMER } from "./types"

const initialState = {
    timerDisplay: '00:00',
    timer: null,
}

export const timerReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_TIMER:
            return {
                ...state,
                timerDisplay: `${action.payload.min}:${action.payload.sec}`,
                timer: parseInt(action.payload.min) * 60 + parseInt(action.payload.sec)
            }
        case START_TIMER:
            return {
                ...state,
                timer: state.timer - 1,
                timerDisplay: `${Math.floor(state.timer / 60)}:${state.timer % 60}`
            }
        case RESET_TIMER: {
            return {
                ...state,
                timer: null,
                timerDisplay: '00:00',
            }
        }
        default:
            return state
    }
}