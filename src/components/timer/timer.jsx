import TimerControls from './timer-controls'
import TimerDisplay from './timer-display'
import './timer.css'

function Timer () {
    return (
        <div className="container-stopwatch black">
            <TimerDisplay></TimerDisplay>
            <TimerControls></TimerControls>
        </div>
    )
}

export default Timer