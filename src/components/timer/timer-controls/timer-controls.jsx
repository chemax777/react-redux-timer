import { useEffect, useState } from "react"
import { RESET_TIMER, SET_TIMER, START_TIMER } from "../../../store/types"
import { useDispatch, useSelector } from "react-redux"

import beepSignal from '../../../media/beep.mp3'

function TimerControls() {
    const [currentTimerMin, setCurrentTimerMin] = useState(0)
    const [currentTimerSec, setCurrentTimerSec] = useState(0)

    const [showInput, setShowInput] = useState(false)
    const [showStart, setShowStart] = useState(false)
    const [showSet, setShowSet] = useState(true) 
    const [showStop, setShowStop] = useState(false)

    const [intervalTimer, setIntervalTimer] = useState()

    const dispatch = useDispatch()
    const timer = useSelector(state => state.timer)
    const display = document.querySelector('.stopwatch-display')

    const showInputTimer = () => {
        showInput ? setShowInput(false) : setShowInput(true)
        dispatch({ type: RESET_TIMER })
        setCurrentTimerMin(0)
        setCurrentTimerSec(0)
    }

    const setTimerMin = (e) => {
        setCurrentTimerMin(e.target.value)
    }

    const setTimerSec = (e) => {
        setCurrentTimerSec(e.target.value)
    }

    const applyTimer = (min, sec) => {
        dispatch({ type: SET_TIMER, payload: { min, sec } })
        setShowInput(false)
        setShowStart(true)
    }

    const startTimer = () => {
        const timerInterval = setInterval(() => {
            dispatch({ type: START_TIMER })
        }, 1000)
        setIntervalTimer(timerInterval)

        showStart ? setShowStart(false) : setShowStart(true)
        showSet ? setShowSet(false) : setShowSet(true)
        showStop ? setShowStop(false) : setShowStop(true)

        display.classList.remove('red')
        display.classList.toggle('green')
    }

    const stopTimer = () => {
        clearInterval(intervalTimer)
        showStart ? setShowStart(false) : setShowStart(true)
        showSet ? setShowSet(false) : setShowSet(true)
        showStop ? setShowStop(false) : setShowStop(true)

        display.classList.remove('green')
        display.classList.toggle('red')
    }

    useEffect(() => {
        if (timer === -1) {
            clearInterval(intervalTimer)
            document.getElementById('signal').play()
            setShowStop(false)
            setShowSet(true)
            dispatch({ type: RESET_TIMER, payload: { min:'', sec: '' } })
            setCurrentTimerMin(0)
            setCurrentTimerSec(0)
            display.classList.remove('green')
        }
    }, [timer, intervalTimer])

    return (
        <>
            <div className="stopwatch-control">
                {showSet && <button onClick={showInputTimer} id="set">Set Timer</button>}
                {showStart && <button onClick={startTimer} id="start">Start Timer</button>}
                {showStop && <button onClick={stopTimer} id="stop">Stop Timer</button>}
            </div>
            {
                showInput &&
                <div className="set-timer-input">
                    <input placeholder="Minutes" type="number" min={0} onChange={(e) => { setTimerMin(e) }} />
                    <input placeholder="Seconds" type="number" min={0} max={"59"} onChange={(e) => { setTimerSec(e) }} />
                    <button onClick={() => applyTimer(currentTimerMin, currentTimerSec)}>Apply Timer</button>
                </div>
            }
            <audio id="signal" src={beepSignal}></audio>
        </>
    )
}

export default TimerControls