import { useSelector } from "react-redux"

function TimerDisplay() {
    const timerDisplay = useSelector(state => state.timerDisplay)

    return (
        <div className="stopwatch-display">
            {timerDisplay}
        </div>
    )
}

export default TimerDisplay